﻿using System;
using ProjectDataSelector.QueryEntities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDataSelector
{
    public class QueryService
    {
        public static async Task<Dictionary<int, int>> GetAmountOfTasksForUser(int userId)
        {
            Dictionary<int, int> amounOfTasks;

            var projects = await RequestService.GetProjects();
            var tasks = await RequestService.GetTasks();

            amounOfTasks = (from project in projects.Where(p => p.AuthorId == userId)
                            join task in tasks on project.Id equals task.ProjectId into taskGroup
                select new
                {
                    ProjectId = project.Id,
                    TaskCount = taskGroup.Count()
                }).ToDictionary(k => k.ProjectId, k => k.TaskCount);

            return amounOfTasks;
        }

        public static async Task<List<Entities.Task>> GetTasksForUser(int userId)
        {
            var tasks = await RequestService.GetTasks();
            var tasksForUser = tasks.Where(t => t.PerformerId == userId && t.Name.Length < 45).ToList();
            return tasksForUser;
        }

        public static async Task<List<TasksIdAndName>> GetFinishedTasksForUser(int userId)
        {
            var tasks = await RequestService.GetTasks();

            var finishedTasksForUser = tasks.Where(t => t.PerformerId == userId && t.FinishedAt.Year == 2019)
                                            .Select(t => new TasksIdAndName()
                {
                    Id = t.Id, 
                    Name = t.Name
                }).ToList();

            return finishedTasksForUser;
        }

        public static async Task<List<TeamIdNameAndListOfUsers>> GetTeamIdNameAndListOfUsers()
        {
            var users = await RequestService.GetUsers();
            var teams = await RequestService.GetTeams();

            var teamUsersInfo = (from user in users
                group user by user.TeamId
                into userGroup
                join team in teams on userGroup.Key equals team.Id
                where userGroup.All(x => (DateTime.Now.Year - x.Birthday.Year) > 12)
                select new TeamIdNameAndListOfUsers()
                {
                    Id = team.Id,
                    Name = team.Name,
                    TeamMembers = userGroup.OrderByDescending(x => x.RegisteredAt).ToList()
                }).ToList();

            return teamUsersInfo;
        }

        public static async Task<List<UsersWithTasks>> GetUsersWithTasks()
        {
            var users = await RequestService.GetUsers();
            var tasks = await RequestService.GetTasks();

            var usersTasksInfo = tasks.GroupBy(t => t.PerformerId).Join(users,
                t => t.Key,
                u => u.Id,
                (t, u) => new UsersWithTasks
                {
                    User = u,
                    Tasks = tasks.Where(task => task.PerformerId == u.Id).OrderByDescending(task => task.Name.Length).ToList()
                }).OrderBy(u => u.User.FirstName).ToList();

            return usersTasksInfo;
        }

        public static async Task<UserProjectsInfo> GetUserProjectsInfo(int userId)
        {
            var user = await RequestService.GetUserById(userId);
            var projects = await RequestService.GetProjects();
            var tasks = await RequestService.GetTasks();
            var taskStates = await RequestService.GetTaskStates();

            var userProjectInfo = new UserProjectsInfo()
            {
                User = user,
                LastProject = projects.Where(p => p.AuthorId == user.Id)
                    .OrderByDescending(p => p.CreatedAt)
                    .FirstOrDefault(),
                tasksNumberForLastProject = tasks.Where(t => t.ProjectId ==
                                                             projects.Where(p => p.AuthorId == user.Id)
                                                                 .OrderByDescending(p => p.CreatedAt)
                                                                 .FirstOrDefault().Id).Count(),
                tasksNumberNotFinishedOrCancelled = tasks.Where(t => t.PerformerId == user.Id).Join(taskStates,
                    t => t.State,
                    ts => ts.Id,
                    (t, ts) => new
                    {
                        State = ts.Value,
                    }).Where(t => t.State == "Canceled" || t.State == "Started").Count(),
                longestInProgressTask = tasks.Where(t => t.PerformerId == user.Id)
                    .OrderBy(t => t.FinishedAt.Subtract(t.CreatedAt)).FirstOrDefault()
            };

            return userProjectInfo;
        }

        public static async Task<ProjectInfo> GetProjectInfo(int projectId)
        {
            var users = await RequestService.GetUsers();
            var project = await RequestService.GetProjectById(projectId);
            var projects = await RequestService.GetProjects();
            var tasks = await RequestService.GetTasks();
            var taskStates = await RequestService.GetTaskStates();

            var userProjectInfo = new ProjectInfo()
            {
                Project = project,
                LongestByDescriptionTask = tasks.Where(t => t.ProjectId == project.Id)
                    .OrderByDescending(t => t.Description.Length)
                    .FirstOrDefault(),
                ShortestByNameTask = tasks.Where(t => t.ProjectId == project.Id)
                    .OrderBy(t => t.Name.Length)
                    .FirstOrDefault(),
                OverallNumberOfUsersWithCondition = tasks.Where(t => t.ProjectId == project.Id
                                                                     && (project.Description.Length > 25 
                                                                         || tasks.Where(task => task.ProjectId == project.Id).Count() < 3)).Count()
            };

            return userProjectInfo;
        }
    }
}
