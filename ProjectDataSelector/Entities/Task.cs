﻿using Newtonsoft.Json;
using System;
using System.Reflection;
using System.Text;

namespace ProjectDataSelector.Entities
{
    public class Task
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("finished_at")]
        public DateTime FinishedAt { get; set; }
        [JsonProperty("state")]
        public int State { get; set; }
        [JsonProperty("project_id")]
        public int ProjectId { get; set; }
        [JsonProperty("performer_id")]
        public int PerformerId { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            var properties = typeof(Task).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                sb.AppendLine(string.Format("{0}: {1}", property.Name, property.GetValue(this)));
            }

            sb.AppendLine();
            return sb.ToString();
        }
    }
}
