﻿using Newtonsoft.Json;

namespace ProjectDataSelector.Entities
{
    public class TaskState
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("value")]
        public string Value { get; set; }
    }
}
