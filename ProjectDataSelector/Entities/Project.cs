﻿using Newtonsoft.Json;
using System;
using System.Reflection;
using System.Text;

namespace ProjectDataSelector.Entities
{
    public class Project
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
        [JsonProperty("author_id")]
        public int AuthorId { get; set; }
        [JsonProperty("team_id")]
        public int TeamId { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            var properties = typeof(Project).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                sb.AppendLine(string.Format("{0}: {1}", property.Name, property.GetValue(this).ToString()));
            }

            sb.AppendLine();
            return sb.ToString();
        }
    }
}