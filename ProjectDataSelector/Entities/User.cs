﻿using Newtonsoft.Json;
using System;
using System.Reflection;
using System.Text;

namespace ProjectDataSelector.Entities
{
    public class User
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("first_name")]
        public string FirstName { get; set; }
        [JsonProperty("last_name")]
        public string LastName { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("birthday")]
        public DateTime Birthday { get; set; }
        [JsonProperty("registered_at")]
        public DateTime RegisteredAt { get; set; }
        [JsonProperty("team_id")]
        public int? TeamId { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            var properties = typeof(User).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                sb.AppendLine(string.Format("{0}: {1}", property.Name, property.GetValue(this).ToString()));
            }

            sb.AppendLine();
            return sb.ToString();
        }
    }
}
