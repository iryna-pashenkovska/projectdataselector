﻿using ProjectDataSelector.Entities;
using ProjectDataSelector.QueryEntities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Task = System.Threading.Tasks.Task;

namespace ProjectDataSelector
{
    public class MenuHandler
    {
        private static void DrawStarLine()
        {
            Console.WriteLine("***********************");
        }
        private static void DrawTitle()
        {
            DrawStarLine();
            Console.WriteLine("+++   PROJECT DATA SELECTOR APP   +++");
            DrawStarLine();
        }
        public static void DrawMenu()
        {
            DrawTitle();
            DrawStarLine();
            Console.WriteLine(" 1. Get number of projects' tasks for particular User (by Id)");
            Console.WriteLine(" 2. Get list of tasks for particular User (by Id), where name's lenght < 45");
            Console.WriteLine(" 3. Get list (id, name) of tasks, which are finished in current (2019) year for particular User (by Id)");
            Console.WriteLine(" 4. Get list (id, team name and user list) from team collection, which members older than 12 years, sorted descending by user's date of registration, and grouped by teams");
            Console.WriteLine(" 5. Get list of users in ascending alphabetical order by first_name with tasks sorted descending by name length");
            Console.WriteLine(" 6. Get following structure (by user id):");
            Console.WriteLine("      - user");
            Console.WriteLine("      - last user's project (by create date)");
            Console.WriteLine("      - overall task number under last project");
            Console.WriteLine("      - ovarall not finished or cancelled user's tasks number");
            Console.WriteLine("      - longest user's task by date (create earliest - finished latest)");
            Console.WriteLine(" 7. Get following structure (by project id):");
            Console.WriteLine("      - project");
            Console.WriteLine("      - longest project's task (by description)");
            Console.WriteLine("      - shortest project's task (by name)");
            Console.WriteLine("      - ovarall number of users in project's team where project description > 25 or number of tasks < 3");
            DrawStarLine();
            Console.WriteLine("Make your choice: type 1, 2,... or {0} for exit", 0);
            DrawStarLine();
        }

        public static async Task<bool> SelectMenuOption(string action)
        {
            bool cont = true;
            switch (action.Trim())
            {
                case "1":
                    await ShowNumberOfTasksPerProjectForUser();
                    break;
                case "2":
                    await ShowTasksForUser();
                    break;
                case "3":
                    await ShowFinishedTasksForUser();
                    break;
                case "4":
                    await ShowTeamIdNameAndListOfUsers();
                    break;
                case "5":
                    await ShowUsersWithTasks();
                    break;
                case "6":
                    await ShowUserProjectsInfo();
                    break;
                case "7":
                    await GetProjectInfo();
                    break;
                case "0":
                    cont = false;
                    break;
            }

            return cont;
        }

        //
        // #1. Get number of projects' tasks for particular User (by Id) 
        // 
        private static async Task ShowNumberOfTasksPerProjectForUser()
        {
            var userId = Convert.ToInt32(AskUser("user ID"));
            Dictionary<int, int> projectsWithTasks = await QueryService.GetAmountOfTasksForUser(userId);
            PrintDictionary("Project ID", "Number of user's tasks", projectsWithTasks);
        }

        //
        // #2. Get list of tasks for particular User (by Id), where name's lenght < 45
        //
        private static async Task ShowTasksForUser()
        {
            var userId = Convert.ToInt32(AskUser("user ID"));
            List<Entities.Task> tasksForUser = await QueryService.GetTasksForUser(userId);
            Print(tasksForUser);
        }

        //
        // #3. Get list (id, name) of tasks, which are finished in current (2019) year for particular User (by Id)
        //
        private static async Task ShowFinishedTasksForUser()
        {
            var userId = Convert.ToInt32(AskUser("user ID"));
            List<TasksIdAndName> tasksForUser = await QueryService.GetFinishedTasksForUser(userId);
            Print(tasksForUser);
        }

        //
        // #4. Get list (id, team name and user list) from team collection, which members older than 12 years, sorted descending by user's date of registration, and grouped by teams
        private static async Task ShowTeamIdNameAndListOfUsers()
        {
            List<TeamIdNameAndListOfUsers> teamIdNameAndListOfUsers = await QueryService.GetTeamIdNameAndListOfUsers();
            PrintTeamIdNameAndListOfUsers(teamIdNameAndListOfUsers);   
        }

        //
        // #5. Get list of users in ascending alphabetical order by first_name with tasks sorted descending by name length
        //
        private static async Task ShowUsersWithTasks()
        {
            List<UsersWithTasks> usersWithTasks = await QueryService.GetUsersWithTasks();
            PrintUsersWithTasks(usersWithTasks);
        }

        //
        // #6. get following structure (by user id):
        //      - user
        //      - last user's project (by create date)
        //      - overall task number under last project
        //      - ovarall not finished or cancelled user's tasks number
        //      - longest user's task by date (create earliest - finished latest)
        //
        private static async Task ShowUserProjectsInfo()
        {
            var userId = Convert.ToInt32(AskUser("user ID"));
            UserProjectsInfo userProjectsInfo = await QueryService.GetUserProjectsInfo(userId);
            Console.WriteLine(userProjectsInfo.ToString());
        }

        //
        // #7. Get following structure (by project id):
        //      - project
        //      - longest project's task (by description)
        //      - shortest project's task (by name)
        //      - ovarall number of users in project's team where project description > 25 or number of tasks < 3
        //
        private static async Task GetProjectInfo()
        {
            var projectId = Convert.ToInt32(AskUser("project ID"));
            ProjectInfo projectInfo = await QueryService.GetProjectInfo(projectId);
            Console.WriteLine(projectInfo.ToString());
        }


        private static string AskUser(string info)
        {
            Console.WriteLine("Please enter {0}: ", info);
            return Console.ReadLine();
        }

        private static void PrintDictionary(string keyFieldName, string valueFieldName, Dictionary<int,int> dictionary)
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<int, int> kvp in dictionary)
            {
                sb.AppendLine(string.Format("{0}: {1}, {2}: {3}", keyFieldName, kvp.Key, valueFieldName, kvp.Value));
            }
            Console.WriteLine(sb);
        }

        static void PrintUsersWithTasks(List<UsersWithTasks> items)
        {
            foreach (var item in items)
            {
                Console.WriteLine("--------{0}--------", typeof(UsersWithTasks).Name);

                Console.WriteLine(item.User.ToString());

                foreach (var task in item.Tasks)
                {
                    Console.WriteLine("--------{0}--------", typeof(Entities.Task).Name);
                    Console.WriteLine(task.ToString());
                    Console.WriteLine("-------------------");
                }

                Console.WriteLine("-------------------");
            }
        }

        static void PrintTeamIdNameAndListOfUsers(List<TeamIdNameAndListOfUsers> items)
        {
            foreach (var item in items)
            {
                Console.WriteLine("--------{0}--------", typeof(TeamIdNameAndListOfUsers).Name);

                Console.WriteLine("Id: {0}", item.Id);
                Console.WriteLine("Name: {0}", item.Name);

                foreach (var user in item.TeamMembers)
                {
                    Console.WriteLine("--------{0}--------", typeof(User).Name);
                    Console.WriteLine(user.ToString());
                    Console.WriteLine("-------------------");
                }

                Console.WriteLine("-------------------");
            }
        }

        static void Print<T>(IEnumerable<T> items)
        {
            var props = typeof(T).GetProperties();

            foreach (var item in items)
            {
                Console.WriteLine("--------{0}--------", typeof(T).Name);
                foreach (var prop in props)
                {
                    Console.WriteLine("{0}: {1}", prop.Name, prop.GetValue(item, null));
                }

                Console.WriteLine("-------------------");
            }
        }
    }
}
