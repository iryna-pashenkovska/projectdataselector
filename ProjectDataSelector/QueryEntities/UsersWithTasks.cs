﻿using ProjectDataSelector.Entities;
using System.Collections.Generic;

namespace ProjectDataSelector.QueryEntities
{
    public class UsersWithTasks
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
