﻿using ProjectDataSelector.Entities;
using System.Reflection;
using System.Text;

namespace ProjectDataSelector.QueryEntities
{
    public class UserProjectsInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int tasksNumberForLastProject { get; set; }
        public int tasksNumberNotFinishedOrCancelled { get; set; }
        public Task longestInProgressTask { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            var properties = typeof(UserProjectsInfo).GetProperties();
            foreach (PropertyInfo property in properties)
            {
                sb.AppendLine(string.Format("{0}: {1}", property.Name, property.GetValue(this).ToString()));
            }

            sb.AppendLine();
            return sb.ToString();
        }
    }
}
