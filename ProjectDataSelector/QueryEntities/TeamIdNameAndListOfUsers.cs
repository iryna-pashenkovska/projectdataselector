﻿using ProjectDataSelector.Entities;
using System.Collections.Generic;

namespace ProjectDataSelector.QueryEntities
{
    public class TeamIdNameAndListOfUsers
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public List<User> TeamMembers { get; set; }
    }
}
